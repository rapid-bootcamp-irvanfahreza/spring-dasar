package com.rapidtech.springdasar.cyclic;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class CyclicC {
    CyclicA cyclicA;
}
